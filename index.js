const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server, Socket } = require("socket.io");
const io = new Server(server);
const PORT = 3000;

app.use(express.json());


/*The list of stock we are tracking in this program */
const stockList = {
  data: [
    { stockName: "ABC Traders", price: 44.51 },
    {
      stockName: "Trendy FinTech",
      price: 23.99,
    },
    {
      stockName: "SuperCorp",
      price: 126.91,
    },
  ],
};

/* When "localhost:3000" site is entered in the url it will trigger this .get and load /index.html from the filetree to the url. */
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});


/* When "localhost:3000/update" site is entered in the url it will trigger this .patch that will read the entered stockname
 and compare it to the stockList and if it doesn't get a match it will go to the "else" statement,
 if the entered stockname do get a match it will update the value of the entered stockname via the "Object.assign"
 and send a updateMessage and update the value live via the io.emit to the browser. */
app.patch("/update", (req, res) => {
  const { data } = req.body;
  const stockChange = stockList["data"].find(e => e["stockName"] === data["stockName"]);
  if (stockChange) {
    Object.assign(stockChange, data);
    res.status(200).json(stockChange);
    io.emit("updateMessage", stockChange);
    switch (data.stockName) {
      case "ABC Traders":
        io.emit("ABC Traders", stockChange);
        break;
      case "Trendy FinTech":
        io.emit("Trendy FinTech", stockChange);
        break;
      case "SuperCorp":
        io.emit("SuperCorp", stockChange);
        break;
    }
  } else {
    res
      .status(404)
      .json({ message: "The stock you try to update does not exist" });
      console.log("The stock you try to update does not exist");
  }
});

/* When "localhost:3000/current" site is entered in the url it will trigger this
 .get and present current stockprices as an json format in the url. */
app.get("/current", (req, res) => {
  res.json(stockList);
});


server.listen(3000, (err) => {
  if (err) console.log(err);
  console.log("Server has started");
});
